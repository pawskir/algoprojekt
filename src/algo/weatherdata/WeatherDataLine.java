package algo.weatherdata;

import java.time.LocalDate;

/**
 * A class for objects that will hold one line of weather data.
 */
public class WeatherDataLine {
    private LocalDate date;
    private String time;
    private double temp;
    private boolean approved;

    public WeatherDataLine(LocalDate date, String time, int temp, boolean approved) {
        this.date = date;
        this.time = time;
        this.temp = temp;
        this.approved = approved;
    }

    public WeatherDataLine(){
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public double getTemp() {
        return temp;
    }

    public void setTemp(double temp) {
        this.temp = temp;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "WeatherDataLine{" +
                "date=" + date +
                ", time='" + time + '\'' +
                ", temp=" + temp +
                ", approved=" + approved +
                '}';
    }
}
