package algo.weatherdata;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
/**
 * Provides methods to retrieve temperature data from a weather station file.    
 */
public class WeatherDataHandler {

	ArrayList<WeatherDataLine> dataLineAL = new ArrayList<>();

	/**
	 * Load weather data from file.
	 * 
	 * @param filePath path to file with weather data
	 * @throws IOException if there is a problem while reading the file
	 */
	public void loadData(String filePath) throws IOException {
		int count = 0;
		List<String> fileData = Files.readAllLines(Paths.get(filePath));

		for (String s: fileData) {
			WeatherDataLine newLine = new WeatherDataLine();
			newLine.setDate(readDate(s));
			newLine.setTemp(readTemp(s));
			newLine.setTime(readTime(s));
			newLine.setApproved(readApproved(s));

			dataLineAL.add(newLine);
			count++;
		}
		System.out.println("\nLoaded " + count + " lines of weather data");
	}


	/**
	 * Search for average temperature for all dates between the two dates (inclusive).
	 * Result is sorted by date. When researching from 2000-01-01 to 2000-01-03 the 
	 * result might be:
	 * 2000-01-01 average temperature: 0.42 degrees Celcius
	 * 2000-01-02 average temperature: 2.26 degrees Celcius
	 * 2000-01-03 average temperature: 2.78 degrees Celcius
	 * 
	 * @param dateFrom start date (YYYY-MM-DD) inclusive  
	 * @param dateTo end date (YYYY-MM-DD) inclusive
	 * @return average temperature for each date, sorted by date  
	 */

	public List<String> averageTemperatures(LocalDate dateFrom, LocalDate dateTo) {
		List<WeatherDataLine> inputList = getPeriod(dateFrom, dateTo);
		List<String> outputList = new ArrayList<>();

		if(inputList == null){
			System.out.println("Incorrect date(s) inserted, try again.");
			outputList.add("");
			return outputList;
		}

		double tempSum = 0;
		int count = 1;
		System.out.println("Average temperatures between " + dateFrom + " and " + dateTo);

		for (int i = 1; i < inputList.size(); i++) {
			if (inputList.get(i).getDate().isEqual(inputList.get(i-1).getDate())){
				count++;
				tempSum += inputList.get(i-1).getTemp();
			}else{
				tempSum += inputList.get(i-1).getTemp();
				tempSum = tempSum / count;
				outputList.add(inputList.get(i-1).getDate() + " average temperature: " + String.format("%.2f", tempSum) + " degrees" + " Celsius");
				tempSum = 0;
				count = 1;
			}
		}
		tempSum += inputList.get(inputList.size() - 1).getTemp();
		tempSum = tempSum / count;
		outputList.add(inputList.get(inputList.size() - 1).getDate() + " average temperature: " + String.format("%.2f", tempSum) + " degrees" + " Celsius");

		return outputList;
	}


	/**
	 * Search for missing values between the two dates (inclusive) assuming there 
	 * should be 24 measurement values for each day (once every hour). Result is
	 * sorted by date. When researching from 2000-01-01 to 2000-01-03 the 
	 * result might be:
	 * 2000-01-01 missing no measurements
	 * 2000-01-02 missing 1 measurements
	 * 2000-01-03 missing 1 measurements
	 * 
	 * @param dateFrom start date (YYYY-MM-DD) inclusive  
	 * @param dateTo end date (YYYY-MM-DD) inclusive
	 * @return dates with missing values together with number of missing values for each date, sorted by date
	 */
	public List<String> missingValues(LocalDate dateFrom, LocalDate dateTo) {
		List<WeatherDataLine> inputList = getPeriod(dateFrom, dateTo);
		List<String> outputList = new ArrayList<>();

		if(inputList == null){
			System.out.println("Incorrect date(s) inserted, try again.");
			outputList.add("");
			return outputList;
		}

		int count = 1;
		int missingValues;
		System.out.println("Missing measurements between " + dateFrom + " and " + dateTo);

		for (int i = 1; i < inputList.size(); i++) {
				if (inputList.get(i).getDate().isEqual(inputList.get(i - 1).getDate())) {
					count++;
				} else {
					missingValues = 24 - count;
					outputList.add(inputList.get(i - 1).getDate() + " " + "missing " + missingValues + " measurements");
					count = 1;
				}
		}
		missingValues = 24 - count;
		outputList.add(inputList.get(inputList.size() - 1).getDate() + " " + "missing " + missingValues + " measurements");

		return outputList;
	}
	/**
	 * Search for percentage of approved values between the two dates (inclusive).
	 * When researching from 2000-01-01 to 2000-01-03 the 
	 * result might be:
	 * Approved values between 2000-01-01 and 2000-01-03: 32.86 %
	 * 
	 * @param dateFrom start date (YYYY-MM-DD) inclusive  
	 * @param dateTo end date (YYYY-MM-DD) inclusive
	 * @return period and percentage of approved values for the period  
	 */
	public List<String> approvedValues(LocalDate dateFrom, LocalDate dateTo) {
		List<WeatherDataLine> inputList = getPeriod(dateFrom, dateTo);
		List<String> outputList = new ArrayList<>();

		if(inputList == null){
			System.out.println("Incorrect date(s) inserted, try again.");
			outputList.add("");
			return outputList;
		}

		float countApproved = 0;
		float count = 0;
		double percent;

		for (WeatherDataLine wDL: inputList) {
			if(wDL.isApproved()){
				countApproved++;
			}
			count++;
		}
		percent = (countApproved / count) * 100;
		outputList.add("Approved values between " + dateFrom + " and " + dateTo + ": " + String.format("%.2f", percent) + "%");
		return outputList;
	}

	/**
	 * Reads the temperature from a line. Goes on index further if there is a "-" present.
	 * @param s one line of weather data
	 * @return temperature reading
	 */
	public double readTemp(String s){
		double temp;
		if(s.substring(20, 21).equals("-")){
			temp = Double.parseDouble(s.substring(20, 24));
		} else {
			temp = Double.parseDouble(s.substring(20, 23));
		}
		return temp;
	}

	/**
	 * Reads a weather data line and looks for the letter "G".
	 * Moves one index further if there is a "-" present.
	 * @param s one line of weather data
	 * @return true if there is a "G" at the given index
	 */
	public boolean readApproved(String s){
		boolean approved = false;
		if(s.substring(20, 21).equals("-")){
			if (s.substring(25).equals("G")) {
				approved = true;
			}
		} else {
			if (s.substring(24).equals("G")){
				approved = true;
			}
		}
		return approved;
	}

	/**
	 * Reads the date from a weather data line. always reads from index 0-10.
	 * @param s one line of weather data
	 * @return date reading
	 */
	public LocalDate readDate(String s){
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		return LocalDate.parse(s.substring(0, 10), formatter);
	}

	/**
	 * Extracts an interval from dataLineAL between the two dates (inclusive)
	 * Returns null if one of the dates are outside of the scope.
	 * @param dateFrom start date (YYYY-MM-DD)
	 * @param dateTo end date (YYYY-MM-DD)
	 * @return a list containing the interval
	 */
	public List<WeatherDataLine> getPeriod(LocalDate dateFrom, LocalDate dateTo){
		if(dateFrom.isBefore(dataLineAL.get(0).getDate()) || dateTo.isAfter(dataLineAL.get(dataLineAL.size() - 1).getDate())){
			return null;
		}
		List<WeatherDataLine> inputList = new ArrayList<>();
		for(int i = getFromDateIndex(dateFrom) ; i <= getToDateIndex(dateTo); i++) {
			inputList.add(dataLineAL.get(i));
		}
		return inputList;
	}

	/**
	 * Finds an index with and object containing the given date.
	 * @param date the date to get an index for (YYYY-MM-DD)
	 * @return an index of an object containing the date.
	 */

	public int binarySearchDateIndex(LocalDate date){
		int low = 0;
		int high = dataLineAL.size() - 1;

		while(low <= high){
			int mid = (low + high) / 2;
			if(dataLineAL.get(mid).getDate().isAfter(date)){
				high = mid - 1;
			} else if (dataLineAL.get(mid).getDate().isBefore(date)){
				low = mid + 1;
			} else {
				return mid;
			}
		}
		return -1;
	}

	/**
	 * There are multiple objects with the same date.
	 * This method looks for the lowest index with the given date.
	 * @param date date to be checked.
	 * @return lowest index containing said date.
	 */
	public int getFromDateIndex(LocalDate date){
		int fromIndex = binarySearchDateIndex(date);

		while (dataLineAL.get(fromIndex - 1).getDate().isEqual(date)){
			fromIndex --;
		}

		return fromIndex;
	}

	/**
	 * There are multiple objects with the same date.
	 * This method looks for the highest index with the given date.
	 * @param date date to be checked.
	 * @return highest index containing said date.
	 */
	public int getToDateIndex(LocalDate date){
		int toIndex = binarySearchDateIndex(date);

		while (dataLineAL.get(toIndex + 1).getDate().isEqual(date)){
			toIndex ++;
		}

		return toIndex;
	}

	/**
	 * Reads the time from a data line. Same index on every line.
	 * @param s one weather data line
	 * @return time reading
	 */
	public String readTime(String s){
		return s.substring(11, 13);
	}
}